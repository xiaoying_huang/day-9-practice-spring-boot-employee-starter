## Objective 事实
- Reviewed MySQL basics, learned how to use MYSQL workbench to visualize.
- Learned JPA (Java Persistence API), which is a specification that provides a convenient way to interact with databases using Java objects.

## Reflective 反应
- I feel that I made good progress in understanding the fundamentals of MySQL and JPA. 
- I gained familiarity with creating and managing database schemas, executing CRUD operations.
- I learned how to utilize the power of JPA to simplify data access and manipulation in Java applications.

## Interpretive 解释/分析
- Mainly utilizing the idea of refactoring and utilizing Jpa's specifications for mapping local repositories to databases. 
- Due to the reuse of a lot of previously learned content, today's practice can be well understood and operated

## Decisional 决定/结果
- Mastering the content learned on that day every day and promptly identifying and filling in gaps can be of great help to advanced learning in the future.